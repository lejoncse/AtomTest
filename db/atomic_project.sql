-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 29, 2015 at 05:46 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomic_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthdate`
--

CREATE TABLE IF NOT EXISTS `birthdate` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `birthday` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `birthdate`
--

INSERT INTO `birthdate` (`id`, `name`, `birthday`) VALUES
(1, '', '0000-00-00'),
(2, '', '0000-00-00'),
(3, 'Anik', '2015-01-01'),
(4, 'sayem', '1993-02-01'),
(5, 'Arif', '1993-04-09');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE IF NOT EXISTS `books` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `title` varchar(2555) NOT NULL,
  `author` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `author`) VALUES
(1, 'fgf', 'ghhgh'),
(2, 'OOP php', 'Mian Zadid Rushdid'),
(3, '', ''),
(4, 'Basic php', 'M Yamin Hossain');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email`) VALUES
(1, 'arif', 'arifcse@gmail.com'),
(2, 'arif', 'arifcse@gmail.com'),
(3, 'abc', 'xyz@gmail.com'),
(4, 'Lejon', 'lejonkhan@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `summarys`
--

CREATE TABLE IF NOT EXISTS `summarys` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `summary` varchar(255) NOT NULL,
  `organisation` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `summarys`
--

INSERT INTO `summarys` (`id`, `summary`, `organisation`) VALUES
(1, '', ''),
(2, '', ''),
(3, 'A new summary', 'Robi'),
(4, 'Gp is a very friendly', 'GP');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
