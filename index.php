<!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <h1>BITM - Web App Dev - PHP</h1>
        <dl>
            <dt><span>Name:</span></dt>
            <dd>BITM BASIS</dd>
            
            <dt>SEIP ID:</dt>
            <dd>SEIP001</dd>
            
            <dt>Batch:</dt>
            <dd>03</dd>
        </dl>
        <h2>Projects</h2>
        <table>
            <thead>
                <tr>
                    <th>Sl.</th>
                    <th>Project Name</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>01</td>
                    <td><a href="./Views/SEIP1020/Birthday/index.php">Birthday</a></td>
                </tr>
                <tr>
                    <td>02</td>
                    <td><a href="./Views/SEIP1020/Book/index.php">Favourite Books</a></td>
                </tr>
                <tr>
                    <td>03</td>
                    <td><a href="./Views/SEIP1020/Email/index.php">Email</a></td>
                </tr>
                <tr>
                    <td>04</td>
                    <td><a href="./Views/SEIP1020/Summary/index.php">Summary</a></td>
                </tr>
            </tbody>
        </table>
        
    </body>
</html>
